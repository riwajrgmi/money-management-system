# Generated by Django 3.1.7 on 2021-03-17 18:04

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('master', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='DepositMethod',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('method', models.CharField(choices=[('Cash', 'Cash'), ('Cheque', 'Cheque'), ('Online', 'Online')], max_length=150)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PersonDetail',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('name', models.CharField(max_length=200)),
                ('contact', models.CharField(blank=True, max_length=10, null=True)),
                ('address', models.CharField(blank=True, max_length=200, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Deposits',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('deposit_date', models.DateTimeField(auto_now=True)),
                ('amount', models.PositiveIntegerField(default=0)),
                ('is_business', models.BooleanField(default=False)),
                ('is_personal', models.BooleanField(default=False)),
                ('source', models.CharField(blank=True, max_length=250, null=True)),
                ('by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='deposit.persondetail')),
                ('deposit_method', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='deposit.depositmethod')),
                ('institution', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='master.institutiondetail')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
